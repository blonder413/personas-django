from django.forms import ModelForm, EmailInput

from personas.models import Persona


class PersonaForm(ModelForm):
    '''
    Personalizamos los campos del formulario
    '''
    class Meta:
        model = Persona
        fields = '__all__'
        widgets = {
            # attrs hace referencia a los atributos HTML
            'email': EmailInput(attrs={'type': 'email'})
        }

