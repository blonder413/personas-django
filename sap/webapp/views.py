from django.forms import modelform_factory
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect

from personas.forms import PersonaForm
from personas.models import Persona

# Create your views here.


def bienvenido(request):
    # return HttpResponse('hola blonder')
    msg1, msg2 = 'hola', 'chao'
    # return render(request, 'webapp/bienvenido.html', {'msg1': msg1, 'msg2': msg2})
    mensajes = {'msg1': 'hola blonder', 'msg2': 'chao blonder'}
    return render(request, 'webapp/bienvenido.html', mensajes)


def personas(request):
    '''
    Listar las personas
    :param request:
    :return:
        HttpResponse
    '''
    no_personas = Persona.objects.count()
    # personas = Persona.objects.all()
    # personas = Persona.objects.order_by('?') # random
    personas = Persona.objects.order_by('-id', 'nombre')
    return render(request, 'webapp/personas.html', {'no_personas': no_personas, 'personas': personas})


def personas_view(request, id):
    # persona = Persona.objects.get(id__exact=id)
    assert isinstance(id, int), 'El id debe ser int' # No es necesario ya que la ruta define int para el parámetro
    # persona = Persona.objects.get(pk=id)
    persona = get_object_or_404(Persona, pk=id)
    return render(request, 'webapp/personas/view.html', {'persona': persona})


def personas_edit(request, id):
    persona = get_object_or_404(Persona, pk=id)
    if request.method == 'POST':
        forma_persona = PersonaForm(request.POST, instance=persona)
        if forma_persona.is_valid():
            forma_persona.save()
            return redirect('webapp_persona')
    else:
        forma_persona = PersonaForm(instance=persona)
    return render(request, 'webapp/personas/edit.html', {'forma_persona': forma_persona})


def personas_delete(request, id):
    persona = get_object_or_404(Persona, pk=id)
    if persona:
        # falta validar la integridad referencial
        persona.delete()
    return redirect('webapp_persona')

def personas_add(request):
    '''
    Creamos el objeto de persona para el formulario
    :param request:
    :return:
    '''
    # persona_form = modelform_factory(Persona, exclude=[])
    if request.method == 'POST':
        forma_persona = PersonaForm(request.POST)
        if forma_persona.is_valid():
            forma_persona.save()
            return redirect('webapp_persona')
    else:
        forma_persona = PersonaForm()
    return render(request, 'webapp/personas/add.html', {'forma_persona': forma_persona})


def despedirse(request):
    return HttpResponse('bye blonder')


def contact(request):
    return HttpResponse('tel: 555-5555555. Email: contacto@email.com')