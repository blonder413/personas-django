from django.urls import path
from .views import bienvenido, personas, personas_view, personas_add, personas_edit, personas_delete

urlpatterns = [
    path('', bienvenido, name='bienvenido'),
    path('personas', personas, name='webapp_persona'),
    path('personas/view/<int:id>', personas_view, name='webapp_personas_view'),
    path('personas/add', personas_add, name='webapp_personas_add'),
    path('personas/edit/<int:id>', personas_edit, name='webapp_personas_edit'),
    path('personas/delete/<int:id>', personas_delete, name='webapp_personas_delete')
]
