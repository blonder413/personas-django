## Instalar django

```
python -m pip install django
django-admin version
```

# Crear proyecto
```
django-admin startproject sap
```

# Desplagarla
```
cd sap
python manage.py runserver
```

# Apps
Una aplicación django puede tener varias app
```
cd sap
python manage.py startapp webapp
se debe registrar entro de /sap/settings.py
```
